# Resolução da tarefa 5
# Identificar intervalos de idades baseado numa lista 

# Dados

# Ana 15, Isabel 28, Rui 27, Pedro 18, Luis 45, Nuno 16, Laura 19, Rodrigo 57 e Beatriz 17

# Opção 1 = Uso de duas listas

nomes = ["Ana", "Isabel", "Rui", "Pedro", "Luis", "Nuno", "Laura", "Rodrigo", "Beatriz"]
idade = [15, 28, 27, 18, 45, 16, 19, 57, 17]

x = 0

for pessoa in nomes:
    if idade[x] < 18:
        print(f"{pessoa} é menor de idade")
    
    elif idade[x] < 35:
        print(f" {pessoa} fica na faixa entre 18 e 35 anos")

    elif idade[x] < 45:
        print(f" {pessoa} fica na faixa entre 35 e 44 anos")

    else:
        print (f"{pessoa} fica na faixa etaria de 45 ou mais amoos")
        x+= 1

