# Identificação faixas etárias

pessoas = ["Ana", "Isabel", "Rui", "Pedro", "Luís", "Nuno",
"Laura", "Rodrigo", "Beatriz"]
idades = [15, 28, 27, 18, 45, 16, 19, 57, 17]

x=0

for pessoa in pessoas:
    print("----------a----------")

if idades [x] < 18:
    print(f"{pessoa} é menor de idade")

elif idades[x] < 35:
    print(f"{pessoa} é jovem adulto")

elif idades [x] < 45:
    print(f"{pessoa} é adulta")

else:
    print(f"{pessoa} é de meia idade")
x += 1

print ("----------")

# Ordenar a lista por ordem alfabetica
print("----------Lista de nomes----------")
pessoas.sort()
print(pessoas)

# Ordenar a lista por ordem crescente de idade
print("----------Lista de idades----------")
idades.sort()
print(idades)

# Criar e organizar tuples

# Organizar os membros por ordem alfabetica
Membros =[("Ana", 15), ("Isabel", 28), ("Rui", 27), ("Pedro", 18), ("Luís", 45), ("Nuno", 16), ("Laura", 19), ("Rodrigo", 57), ("Beatriz", 17)]
Membros.sort(key = lambda x: x[0])
print ("----------Lista de membros por ordem alfabetica----------")
print(Membros)

# Ordenar os membros por ordem crescente de idade
Membros.sort (key = lambda x: x[1])
print(f"----------Lista de membros por ordem crescente----------")
print(Membros)

# Ordenar os membros por ordem decrescente de idade
Membros.sort (key = lambda x: x [1], reverse=True)
print("----------Lista de membros por ordem decrescente----------")
print(Membros)