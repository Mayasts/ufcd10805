# Criar dicionários

pessoas = [{"nome": "Ana", "idade": 15}, {"nome": "Isabel", "idade": 28}, {"nome": "Rui", "idade": 27}, {"nome": "Pedro", "idade": 18}, {"nome": "Luís", "idade": 45}, {"nome": "Nuno", "idade": 16}, {"nome": "Laura", "idade": 19}, {"nome": "Rodrigo", "idade": 57}, {"nome": "Beatriz", "idade": 17}]
x = 0

# a) Organizar as pessoas por faixa etária
print(f"-------a)-------")
for individuo in pessoas:

    if individuo['idade'] < 18:
        print(f"{individuo['nome']} é menor de idade")
    
    elif individuo['idade'] < 35:
        print(f"{individuo['nome']} é jovem")

    elif individuo['idade'] < 45:
        print(f"{individuo['nome']} é jovem adulto")

    else:
        print(f"{individuo['nome']}  é adulto")
    x += 1

# b) Ordenar lista de dicionário por ordem alfabética
pessoas_ordenadas = sorted(pessoas, key=lambda obj: obj["nome"])
print(f"-----------------b)-----------------")
for pessoa in pessoas_ordenadas:
    print(f"{pessoa['nome']} tem {pessoa['idade']} anos.")

# c) Ordenar lista de dicionário por ordem crescente de idade
pessoas_ordenadas = sorted(pessoas, key=lambda obj: obj["idade"])
print(f"-----------------c)-----------------")
for pessoa in pessoas_ordenadas:
    print(f"{pessoa['nome']} tem {pessoa['idade']} anos.")

# d) Ordenar lista de dicionário por ordem decrescente de idade
pessoas_ordenadas = sorted(pessoas, key=lambda obj: obj["idade"], reverse=True)
print(f"------------d)------------")
for pessoa in pessoas_ordenadas:
    print(f"{pessoa['nome']} tem {pessoa['idade']} anos.")

    
