# Adicionar IVA
iva_imposto = float (input('insira o imposto sobre o valor agregado'))
valor_liquido = float (input ('insira o valor liquido do imovel'))

# Valor do IVA
Iva = (iva_imposto * valor_liquido) / 100
valor_bruto = Iva + valor_liquido

print('\n ========== Resultado ========== \n')
print('seu imposto IVA : R€ {:,.2f}'.format(Iva))
print('valor liquido : R€ {:,.2f}'.format(valor_liquido))
print('valor bruto : R€ {:,.2f}'.format(valor_bruto))