 # Descrição da aplicação

 # Colocamos biblioteca que vamos utilizar
 
import math 
import os 

 # Variáveis
valor_imovel = 150000 # número Inteiro
funcionarios = ( "Rui", "Manuel", "Luisa", "Ana") # Lista
taxas_retencao = (0.0, 0.1, 0.2, 0.3) # Lista de Salários
salarios = (750, 967, 1115, 710) # Lista de salários
boas_vindas = "Bem - vindas ao programa de cálculo de impostos" # string


# Funções

# função sem retorno e sem parâmetro
def msg_inicial():
    print(boas_vindas)

# Função sem retorno e com parâmetros

def calculo_dificil(a,b,c):
    z = a + b - c
    print(f"O resultado é {z}")

# Função com retorno e com parâmetros

def calculo_simples():
    valor1 = 10
    valor2 = 5
    resultado = valor1 + valor2
    return resultado

   

# Função sem retorno e com parâmetros

def calculo_dificil2(a,b,c):
    z = a + b - c
    return z


    # Bloco principal 
os.system ('cls') #CLS é para windows
msg_inicial()
calculo_dificil(valor_imovel, 8, 3)
resultado_da_conta = calculo_simples()
print(f"O resultado da conta é {resultado_da_conta}")
resultado_nova_conta = calculo_dificil2 (10, 5, 2)
print(f"O resultado da nova conta é {resultado_nova_conta}")

#Utilização da biblioteca math

valor = 19.54

valor_arredondado = math.floor(valor) # Arredonda para baixo
print(f"O valor arredondado para baixo é {valor_arredondado}")
valor_arredondado = math.ceil(valor) # Arredonda para cima
print(f"O valor arredondado para cima é {valor_arredondado}")




    

