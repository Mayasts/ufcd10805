
funcionarios = ["Joao","André","Fatima", "Carlos", "Joana", "Francisca"]
salarios = [715.04, 1723.65, 917.45, 1167.26, 1265.50, 2143.98]
x = 0

for salario in salarios:
    if salario <900:
        salario = salario 
        print(f"O Salario liquido do/a {funcionarios[x]} após o imposto é {salario}")

    if salario >= 900 and salario < 1100:
        salario = salario - (salario * 0.05)
        print(f"O Salario liquido do/a {funcionarios[x]} após o imposto é {salario}")

    if salario >= 1100 and salario < 1500:
         salario = salario - (salario * 0.10)
         print(f"O Salario liquido do/a {funcionarios[x]} após o imposto é {salario}")

    if salario >= 1500:
        salario = salario - (salario * 0.20)
        print(f"O Salario liquido do/a {funcionarios[x]} após o imposto é {salario}")
        x += 1

