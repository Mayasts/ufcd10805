# constantes
Taxa_IVA = 0.23
Taxa_de_comissao_imobiliaria = 0.05


# Variavéis
Valor_do_imóvel = 150000.00

#Adicionar IVA
iva_imposto = float(input('insira o imposto sobre o valor agregado '))
valor_liquido = float(input('insira o valor liquido do imóvel '))

# Valor do IVA
Iva = (iva_imposto + valor_liquido) / 100
Valor_bruto = Iva + valor_liquido

print('\n ========== Resultado ==========\n')
print('Imposto IVA : R€ {:,.2f}'.format(Iva))
print('Valor Liquido : R€ {:,.2f}'.format(valor_liquido))
print('Valor Bruto : {:,.2f}'.format(Valor_bruto))